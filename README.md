Bank
=====


Run Locally
-----------

Tested using Docker and Docker-Compose.

- docker version 17.09.1-ce
- docker-compose version 1.16.1

To run just:

1 - Install dependencies

```
docker-compose -f dev.yml build --force-rm
```

2 - Start

```
docker-compose -f dev.yml up --force-recreate
```


3 - The first time you run you will encounter a few errors regarding django connecting to postgres but you don't have to worry about that, just keep following the steps.
All you have to do is wait for the following to appear on your console (it means postgres started successfully the first time and that is now ready to accept django connections and then run the command bellow to clean and restore a new DB).

```
bank_postgis_1  | PostgreSQL init process complete; ready for start up.
bank_postgis_1  |
bank_postgis_1  | LOG:  database system was shut down at 2017-06-13 16:42:04 UTC
bank_postgis_1  | LOG:  MultiXact member wraparound protections are now enabled
bank_postgis_1  | LOG:  database system is ready to accept connections
bank_postgis_1  | LOG:  autovacuum launcher started
```

4 - While the server is running, open a new terminal and follow the following commands to clean db and apply the django migrations ( https://github.com/psychok7/django-yadpt-starter/issues/13 )

```
docker-compose -f dev.yml run --rm bank_web bash # Go inside the container
python3 manage.py clear_db --settings=bank.settings.local
python3 manage.py migrate --settings=bank.settings.local
```

5 - Create a superuser account inside the container:

```
python3 manage.py createsuperuser --settings=bank.settings.local
```

6 - Restart the server by stopping it and run:

```
docker-compose -f dev.yml up --force-recreate
```


7 - Go to http://localhost:8000/bank-secret-admin and authenticate yourself and see the database models. We are using honeypot to fool the hackers when they try to access our /admin.

8 - The API will be at http://localhost:8000/api/

9 - Run the available test suite on a separate console.

```
docker-compose -f dev.yml run --rm bank_web bash # Go inside the container
python3 manage.py test --settings=bank.settings.test
```


Registration and Authentication
-------------------------------

We are using JWT Tokens to authenticate in our API (https://github.com/GetBlimp/django-rest-framework-jwt).


### POST /user/register/

#### Summary

Register a new user.

#### Payload

- name: name of the user.
- email: email of the user.
- password: password of the user.
- phone (optional): phone number of the user.

###### Example   
```
            {
                "name" : "khan",
                "email": "nuno@example.com",
                "password": "12we123",
                "phone": "+35196282281"
            }
```

#### Reply

- id: id of the user.
- email: email of the user.
- token: jwt-token of the user.

###### Example
```
{
    "id": 2,
    "email": "nuno@example.com",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJ1c2VybmFtZSI6Im51bm9AZXhhbXBsZS5jb20iLCJleHAiOjE1MTQ1NzA1NDAsImVtYWlsIjoibnVub0BleGFtcGxlLmNvbSJ9.WTb8z6_G234_t6PUdkdY4dCKokr93zx8zpgnz2HWG4c"
}
```


### POST /user/login/

#### Summary

Login user.

#### Payload

- email: email of the user.
- password: password of the user.

###### Example   
```
            {
                "email": "nuno@example.com",
                "password": "12we123"
            }
```

#### Reply

- token: jwt-token of the user.

###### Example
```
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJ1c2VybmFtZSI6Im51bm9AZXhhbXBsZS5jb20iLCJleHAiOjE1MTQ1NzA4NTMsImVtYWlsIjoibnVub0BleGFtcGxlLmNvbSJ9.E0BKWtz181aLxyldeShZvQr2xPKtDS5BOyMQtzENI9A"
}
```


Loans
------


After we get the JWT token we must authenticate our requests with the following HEADER:

```
authorization:JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjozNywidXNlcm5hbWUiOiJjb21lcmNpYWxAZXhhbXBsZS5jb20iLCJleHAiOjE1MTQ5OTUzMDYsImVtYWlsIjoiY29tZXJjaWFsQGV4YW1wbGUuY29tIiwib3JpZ19pYXQiOjE1MTQzOTA1MDZ9.NJX-tzNIZn68Juhai85_kKFuLOlTcmQ8lc9peeNm-E4
```

### POST /loan

#### Summary

Creates a loan application. Loans are automatically accepted.

#### Payload
- amount: loan amount in dollars.
- term: number of months that will take until its gets paid-off.
- rate: interest rate as decimal.
- date: when a loan was asked (origination date as an ISO 8601 string).

###### Example
```
{
	"amount": 1000,
	"term": 12,
	"rate": 0.05,
	"date": "2017-08-05 02:18Z"
}
```

#### Reply

- id: the internal db id
- user: the id of the requesting user
- loan_id: unique id of the loan.
- installment: monthly loan payment.
- amount: loan amount in dollars.
- term: number of months that will take until its gets paid-off.
- rate: interest rate as decimal.
- date: when a loan was asked (origination date as an ISO 8601 string).

###### Example

```
        {
            "id": 8,
            "loan_id": "c619b755-849c-4cff-a450-3b511adf6417",
            "installment": 85.60,
            "user": 2,
            "amount": "1000",
            "term": 12,
            "rate": "0.05",
            "date": "2017-08-05T02:18:00Z"
        }
```

### POST /loan/<:id>/payments

#### Summary

Creates a record of a payment `made` or `missed`.

#### Payload

- payment: type of payment: `made` or `missed`.
- date: payment date.
- amount: amount of the payment `made` or `missed` in dollars.

###### Example (Payment made)
```
{
  "payment": "made",
  "date": "2017-09-05 02:18Z",
  "amount": 85.60
}
```
###### Example (Payment missed)
```
{
  "payment": "missed",
  "date": "2017-09-05 02:18Z",
  "amount": 85.60
}
```

#### Reply

- id: the internal db id
- loan: the internal db id of the loan
- payment: type of payment: `made` or `missed`.
- date: payment date.
- amount: amount of the payment `made` or `missed` in dollars.

###### Example
```
            {
                "id": 1,
                "loan": 1,
                "payment": "made",
                "amount": "85.60",
                "date": "2017-09-05T02:18:00Z",
                "created_on": "2017-12-29T22:33:02.631321Z",
                "updated_on": "2017-12-29T22:33:02.631344Z"
            }
```

### GET /loan/<:id>/balance

#### Summary

Get the volume of outstanding debt (i.e., debt yet to be paid) at some point in time.

#### Payload

- start_date: loan balance from this date.
- end_date: loan balance until this date.

###### Example   
```
/loan/<:id>/balance/?start_date=2016-09-05 02:18Z&end_date=2018-09-05 02:18Z
```

#### Reply

- balance: outstanding debt of loan.

###### Example
```
{
	"balance": 40
}
```

### GET /loan/total_amount

#### Summary

Keep track of the amount of money loaned out.

#### Payload

- start_date: amount from this date.
- end_date: amount until this date.

###### Example   
```
/loan/total_amount?start_date=2016-09-05 02:18Z&end_date=2018-09-05 02:18Z
```

#### Reply

- total_amount: total amount loaned.

###### Example
```
{
	"total_amount": 4023
}
```

### GET /loan/<:id>/missed_made_payments

#### Summary

Keep track of the amount of the missed/made payments.

#### Payload

- start_date: amount of the missed/made payments from this date.
- end_date: amount of the missed/made payments until this date.

###### Example   
```
/loan/<:id>/missed_made_payments?start_date=2016-09-05 02:18Z&end_date=2018-09-05 02:18Z
```

#### Reply

- loan_id: the id of the loan.
- made: made payments.
- missed: missed payments.

###### Example
```
            {
                "loan_id": "7348a345-2887-4db7-9a59-c33288d07e24",
                "made": 1,
                "missed": 1
            }
```


API Docs
---------

When you are authenticated in the admin (make sure it's the same browser), you can have a view the auto-generated API documentation http://localhost:8000/api/docs. Please use your favorite REST client instead as the one in the auto generated docs is not tested.


Encryption
----------

As a proof of concept only, here is a management command that encrypts a few attributes of a Loan (we are not saving them)
```
python3 manage.py encrypt --settings=bank.settings.local
```


Run Tests
---------

# There is a special setting file for the tests.
```
python3 -Wall manage.py test --noinput --settings=bank.settings.test

or

python3 -Wall manage.py test --keepdb --noinput --settings=bank.settings.test
```
