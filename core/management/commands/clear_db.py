# -*- coding: utf-8 -*-

import os
import subprocess

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Deletes Everything inside a Postgres Database!'

    def handle(self, *args, **options):
        name = settings.DATABASES['default']['NAME']
        user = settings.DATABASES['default']['USER']
        os.environ["PGPASSWORD"] = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']

        command = (
            f"psql --host={host} --port={port} --user={user} --no-password "
            f"-c 'drop schema IF EXISTS public cascade; "
            f"DROP SCHEMA IF EXISTS topology cascade; "
            f"DROP SCHEMA IF EXISTS tiger cascade; "
            f"DROP SCHEMA IF EXISTS tiger_data cascade; "
            f"create schema public; CREATE EXTENSION postgis; "
            f"CREATE EXTENSION postgis_topology;'"
        )

        self.stdout.write('Running PSQL command: ')
        self.stdout.write(command)

        subprocess.call([command], shell=True)

        self.stdout.write('Finished deleting ALL database content!!')
