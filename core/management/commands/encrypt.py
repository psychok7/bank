# -*- coding: utf-8 -*-

import os

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from cryptography.fernet import Fernet

from loans.models import Loan


class Command(BaseCommand):
    help = "Encrypt Loans"

    @transaction.atomic
    def handle(self, *args, **options):
        f = Fernet(os.environ.get('FERNET_ENCRYPTION_KEY'))

        for loan in Loan.objects.all():
            self.stdout.write('loan id: "%s"' % loan.id)
            self.stdout.write('loan amount: "%s"' % loan.amount)
            self.stdout.write('loan term: "%s"' % loan.term)

            amount_token = f.encrypt(str(loan.amount).encode('utf-8'))
            self.stdout.write('amount_token: %s' % amount_token)

            term_token = f.encrypt(str(loan.term).encode('utf-8'))
            self.stdout.write('term_token: %s' % term_token)

            self.stdout.write('*******************************************')

        self.stdout.write('Finished Encrypting')




