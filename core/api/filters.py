# -*- coding: utf-8 -*-

import django_filters

from django_filters.fields import Lookup


# Based on https://github.com/carltongibson/django-filter/issues/137#issuecomment-38158832
class ListFilter(django_filters.Filter):
    def filter(self, qs, value):
        value_list = value.split(u',')
        return super(ListFilter, self).filter(qs, Lookup(value_list, 'in'))


