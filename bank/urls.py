# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin


from rest_framework.documentation import include_docs_urls
from rest_framework_jwt.views import refresh_jwt_token

# Viewsets.
# Based on https://github.com/encode/django-rest-framework/issues/2136
from core.api.viewsets import *
from users.api.viewsets import *
from loans.api.viewsets import *

from bank import router

admin.site.site_header = 'Bank (BackOffice)'
admin.autodiscover()

API_TITLE = 'Bank API'
API_DESCRIPTION = 'A clean API for Bank'

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(
        r'^api/docs/',
        include_docs_urls(title=API_TITLE, description=API_DESCRIPTION)
    ),
    # url(r'^api/api-token-auth/', obtain_jwt_token, name='api-token-auth'),
    url(
        r'^api/api-token-refresh/',
        refresh_jwt_token, name='api-token-refresh'
    ),
    url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    url(r'^bank-secret-admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
