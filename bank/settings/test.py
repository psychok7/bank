from .base import *

BASE_URL = 'http://localhost:8000'

SECRET_KEY = os.environ.get('TEST_SECRET_KEY', '')
