import datetime

from .base import *

BASE_URL = 'http://localhost:8000'

# For testing only
# JWT_AUTH = {
#     'JWT_EXPIRATION_DELTA': datetime.timedelta(days=1),
# }

SECRET_KEY = os.environ.get('LOCAL_SECRET_KEY', '')

