# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from users.factories import UserFactory


def get_user_token(instance, another_user=False):
    """
    Get user Token
    """
    data = {"password": 'default_password'}
    if not another_user:
        data["email"] = instance.user.email
    else:
        data["email"] = instance.another_user.email

    response = instance.client.post(
        reverse('user-login'), data, format='json'
    )
    instance.assertEqual(response.status_code, status.HTTP_200_OK)
    instance.assertTrue(response.data)
    return response.data['token']


class UserAPITests(TestCase):
    def setUp(self):
        self.client = APIClient(enforce_csrf_checks=True)
        self.user = UserFactory()

    def test_201_create_user(self):
        """
        Ensure we can register a new user.
        """
        data = {
            "name": "test2", "email": "test2@example.com",
            "password": "default_password", "phone": "962826981"
        }
        response = self.client.post(
            reverse('user-register'), data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        user_id = response.data.get('id')
        if user_id is None:
            self.fail("user_id was not generated!")

        api_token = response.data.get('token')
        if api_token is None:
            self.fail("api_token was not generated!")

        user = UserFactory(email=data['email'])

        self.assertEqual(user.email, data['email'])
        self.assertEqual(response.data['token'], api_token)
        self.assertTrue(response.data)

    def test_200_login(self):
        """
        Ensure we can login.
        """
        data = {"email": self.user.email, "password": "default_password"}
        response = self.client.post(
            reverse('user-login'), data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        api_token = response.data.get('token')
        if api_token is None:
            self.fail("api_token was not generated!")

        self.assertEqual(response.data, {'token': api_token})
        self.assertTrue(response.data)

