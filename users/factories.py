# -*- coding: utf-8 -*-

import factory

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group


class GroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Group
        django_get_or_create = ('name',)

    name = factory.Sequence(lambda n: "Group #%s" % n)


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = get_user_model()
        django_get_or_create = ('email',)

    is_superuser = False
    is_staff = False
    name = factory.Faker('name', locale='pt_PT')
    email = factory.Faker('email')
    password = factory.PostGenerationMethodCall(
        'set_password', 'default_password')


class AdminFactory(UserFactory):
    name = 'admin'
    is_superuser = True
    is_staff = True

