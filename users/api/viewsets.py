# -*- coding: utf-8 -*-

import logging

from datetime import datetime

from rest_framework import viewsets, status
from rest_framework.decorators import list_route
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework_jwt.settings import api_settings

from bank import router
from users.api.authentication import NoAuthentication
from users.api.serializers import UserRegistrationSerializer

log = logging.getLogger('bank_console')


class UserViewSet(viewsets.ViewSet):

    def get_jwt_token(self, user):
        log.info('User {user} requested a JWT Token'.format(**locals()))
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        log.info('JWT Token Generated'.format(**locals()))

        return token

    @list_route(
        methods=['post'], permission_classes=[AllowAny],
        authentication_classes=[NoAuthentication]
    )
    def login(self, request):
        """
        API endpoint that allows a user to login with the email.

        <b>Request Example</b>
        <pre><code>
            {"email":"nuno@example.com","password":"123"}
        </code></pre>

        <b>Response Example</b>
        <pre><code>
            {
              "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJ1c2VybmFtZSI6Im5raGFuQHViaXdoZXJlLmNvbSIsImV4cCI6MTUwNDAzNjU5NiwiZW1haWwiOiJua2hhbkB1Yml3aGVyZS5jb20iLCJvcmlnX2lhdCI6MTUwMzQzMTc5Nn0.mKTAw8AyHkMwtOo6iju9NAbXknVZ9XJjdMpk0QEMJTw"
            }
        </code></pre>

        """
        serializer = JSONWebTokenSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER
            response_data = jwt_response_payload_handler(token, user, request)
            response = Response(response_data)
            if api_settings.JWT_AUTH_COOKIE:
                expiration = (
                        datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA)
                response.set_cookie(
                    api_settings.JWT_AUTH_COOKIE, token, expires=expiration,
                    httponly=True)
            return response

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(
        methods=['post'], permission_classes=[AllowAny],
        authentication_classes=[NoAuthentication]
    )
    def register(self, request):
        """
        API endpoint that allows user registration.

        <b>Request Example</b>
        <pre><code>
            {
                "name" : "khan",
                "email": "nuno@example.com",
                "password": "12we123",
                "phone": "+35196282281"
            }
        </code></pre>

        <b>Response Example</b>
        <pre><code>
            {
              "id": 17,
              "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Im5raGFuQHViaXdoZXJlLmNvbSIsImV4cCI6MTQ5Njc2NTkzMywidXNlcl9pZCI6MTcsImVtYWlsIjoibmtoYW5AdWJpd2hlcmUuY29tIn0.5Q4infnz2wiC3TD22JtP_SBOg_w0SMLNRj7WbON2ojk",
              "email": "nkhan@ubiwhere.com"
            }
        </code></pre>

        """
        serializer = UserRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            instance = serializer.save()
            data = {
                'id': instance.id, 'email': instance.email,
                'token': self.get_jwt_token(user=instance)
            }

            return Response(data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


router.register(r'user', UserViewSet, base_name='user')
