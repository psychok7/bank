# -*- coding: utf-8 -*-

import logging

from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from users.models import UserProfile

log = logging.getLogger('bank_console')


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'name', 'password', 'last_login',)
        extra_kwargs = {'password': {'write_only': True, }, }


class UserRegistrationSerializer(serializers.Serializer):
    name = serializers.CharField()
    email = serializers.EmailField()
    password = serializers.CharField(write_only=True)
    phone = serializers.CharField(required=False)

    def validate_email(self, value):
        if get_user_model().objects.filter(email__iexact=value).exists():
            raise serializers.ValidationError(
                _('This Email is already being used')
            )

        return value

    def create(self, validated_data):
        profile_data = {}
        if validated_data.get('phone') is not None:
            profile_data['phone'] = validated_data.pop('phone')

        validated_data['password'] = make_password(validated_data['password'])
        user = get_user_model().objects.create(**validated_data)
        # We have a signal that creates a user profile as well.
        UserProfile.objects.update_or_create(user=user, defaults=profile_data)

        return user




