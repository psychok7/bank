#!/bin/sh

python3 manage.py makemigrations --settings=bank.settings.production
python3 manage.py migrate --settings=bank.settings.production
python3 manage.py collectstatic --noinput --settings=bank.settings.production
# worker = 2 * CPUs + 1.
gunicorn bank.wsgi:application -w 5 --worker-class 'gevent' --log-level debug --access-logfile logs/gunicorn/access.log --log-file logs/gunicorn/gunicorn.log -e DJANGO_SETTINGS_MODULE=bank.settings.production -b :8000

#python3 manage.py runserver 0.0.0.0:8000 --settings=bank.settings.production