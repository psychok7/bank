# -*- coding: utf-8 -*-

import uuid
import logging

from decimal import Decimal, ROUND_DOWN
from django.contrib.gis.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from dry_rest_permissions.generics import allow_staff_or_superuser, \
    authenticated_users

log = logging.getLogger('bank_console')


class Loan(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.PROTECT,
        verbose_name=_('user')
    )

    is_accepted = models.BooleanField(
        default=True, verbose_name=_('is accepted'))

    loan_id = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)

    amount = models.DecimalField(
        max_digits=10, decimal_places=0, verbose_name=_('amount')
    )
    term = models.PositiveIntegerField(default=12)
    rate = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_('rate')
    )
    date = models.DateTimeField(verbose_name=_('date'))

    created_on = models.DateTimeField(
        auto_now_add=True, verbose_name=_('created_on'))
    updated_on = models.DateTimeField(
        auto_now=True, verbose_name=_('updated_on'))

    def __str__(self):
        return str(self.loan_id)

    def truncate_decimal(self, d, places):
        """
        https://stackoverflow.com/a/41523702/977622

        Truncate Decimal d to the given number of places.

        truncate_decimal(Decimal('85.60748178846711454709901766'), 2)
        Decimal('85.60')

        """
        return d.quantize(Decimal(10) ** -places, rounding=ROUND_DOWN)

    @property
    def installment(self):
        # Monthly installment
        r = self.rate / 12
        return self.truncate_decimal(
            (r + r / ((1 + r) ** self.term - 1)) * self.amount, 2
        )

    @staticmethod
    @authenticated_users
    @allow_staff_or_superuser
    def has_read_permission(request):
        log.info('loan has_read_permission')
        return True

    @authenticated_users
    def has_object_read_permission(self, request):
        log.info('loan has_object_read_permission')
        if request.user.is_superuser or request.user.is_staff:
            return True
        return request.user == self.user

    @staticmethod
    @authenticated_users
    # user/admin can only create and cannot perform any other type of write
    def has_write_permission(request):
        log.info('loan has_write_permission')
        return False

    @staticmethod
    @authenticated_users
    # create is a specific action and therefore takes precedence over write
    # and gives all users the ability to create projects.
    def has_create_permission(request):
        log.info('loan has_create_permission')
        return True

    @staticmethod
    @authenticated_users
    def has_payments_permission(request):
        log.info('loan has_payments_permission')
        return True

    @authenticated_users
    def has_object_payments_permission(self, request):
        log.info('loan has_object_payments_permission')
        return request.user == self.user

    class Meta:
        verbose_name = _('loan')
        verbose_name_plural = _('loans')


class Payment(models.Model):
    PAYMENT_TYPES = (
        ('made', _('made')),
        ('missed', _('missed')),
    )

    loan = models.ForeignKey(
        'loans.Loan', on_delete=models.CASCADE, verbose_name=_('loan')
    )

    payment = models.CharField(
        _('payment'), db_index=True, max_length=10, choices=PAYMENT_TYPES,
        default='made'
    )

    amount = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name=_('amount')
    )

    date = models.DateTimeField(verbose_name=_('date'))

    created_on = models.DateTimeField(
        auto_now_add=True, verbose_name=_('created_on'))
    updated_on = models.DateTimeField(
        auto_now=True, verbose_name=_('updated_on'))

    def __str__(self):
        return str(self.loan)

    @staticmethod
    @authenticated_users
    @allow_staff_or_superuser
    def has_read_permission(request):
        log.info('payment has_read_permission')
        return True

    @authenticated_users
    def has_object_read_permission(self, request):
        log.info('payment has_object_read_permission')
        if request.user.is_superuser or request.user.is_staff:
            return True
        return request.user == self.loan.user

    @staticmethod
    @authenticated_users
    # user/admin can only create and cannot perform any other type of write
    def has_write_permission(request):
        log.info('payment has_write_permission')
        return False

    @staticmethod
    @authenticated_users
    # create is a specific action and therefore takes precedence over write
    # and gives all users the ability to create projects.
    def has_create_permission(request):
        log.info('payment has_create_permission')
        return True

    @staticmethod
    @authenticated_users
    def has_payments_permission(request):
        log.info('payment has_payments_permission')
        return True

    @authenticated_users
    def has_object_payments_permission(self, request):
        log.info('payment has_object_payments_permission')
        return request.user == self.loan.user

    class Meta:
        verbose_name = _('payment')
        verbose_name_plural = _('payment')
