# -*- coding: utf-8 -*-

from rest_framework import permissions


class IsOwnerCanEdit(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Notice the POST request method is left outside because its used to CREATE.
    """

    def has_object_permission(self, request, view, obj):
        if request.method == 'PATCH' or request.method == 'PUT' or request.method == 'DELETE':
            return request.user.is_authenticated() and request.user == obj.owner

        return True
