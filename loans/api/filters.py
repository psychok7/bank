# -*- coding: utf-8 -*-

import logging

from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

from dry_rest_permissions.generics import DRYPermissionFiltersBase

log = logging.getLogger('bank_console')


class LoanFilterBackend(DRYPermissionFiltersBase):
    """
    A filter backend that limits results to those where the requesting user
    has read object level permissions.
    """

    def filter_list_queryset(self, request, queryset, view):
        if not request.user.is_superuser and not request.user.is_staff:
            # Normal user can only view his loans
            filters = {'user': request.user}
            queryset = queryset.filter(**filters)

        return queryset


class PaymentFilterBackend(DRYPermissionFiltersBase):
    """
    A filter backend that limits results to those where the requesting user
    has read object level permissions.
    """

    def filter_list_queryset(self, request, queryset, view):
        if not request.user.is_superuser and not request.user.is_staff:
            # Normal user can only view his loans
            filters = {'loan__user': request.user}
            queryset = queryset.filter(**filters)

        return queryset
