# -*- coding: utf-8 -*-

import logging

from django.db.models import Sum

from rest_framework import status, mixins
from rest_framework.decorators import list_route, detail_route
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.viewsets import GenericViewSet

from dry_rest_permissions.generics import DRYPermissions

from bank import router
from loans.api.filters import LoanFilterBackend, PaymentFilterBackend

from loans.api.serializers import LoanSerializer, PaymentSerializer, \
    DateSerializer
from loans.models import Loan, Payment

log = logging.getLogger('bank_console')


class LoanViewSet(mixins.CreateModelMixin, GenericViewSet):
    """
    Creates a loan application. Loans are automatically accepted.

    create:
    Creates a Loan.

    <b>Parameters (not automatically generated):</b>

        - amount: loan amount in dollars.

        - term: number of months that will take until its gets paid-off.

        - rate: interest rate as decimal.

        - date: when a loan was asked (origination date as an ISO 8601 string).

    <b>Request Example</b>
    <pre><code>
        {
            "amount": 1000,
            "term": 12,
            "rate": 0.05,
            "date": "2017-08-05 02:18Z",
        }
    </code></pre>

    <b>Response Example</b>
    <pre><code>
        {
            "id": 8,
            "loan_id": "c619b755-849c-4cff-a450-3b511adf6417",
            "installment": 85.60,
            "user": 2,
            "amount": "1000",
            "term": 12,
            "rate": "0.05",
            "date": "2017-08-05T02:18:00Z"
        }
    </code></pre>

    """
    queryset = Loan.objects.all()

    serializer_class = LoanSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)

    # This function exists so that we can manually enforce permissions over
    # models in routes that are not part of a modelserializer serializer_class
    def _manually_check_permission(self, request, obj):
        for permission in self.get_permissions():
            if not permission.has_permission(request, self):
                return False

        for permission in self.get_permissions():
            if not permission.has_object_permission(request, self, obj):
                return False

        return True

    @detail_route(
        methods=['post'], authentication_classes=[JSONWebTokenAuthentication],
        permission_classes=[IsAuthenticated, DRYPermissions],
        serializer_class=PaymentSerializer
    )
    def payments(self, request, pk=None):
        """
        Creates a record of a payment made or missed.

        create:
        Creates a payment associated to a loan.

        <b>Parameters (not automatically generated):</b>

            - payment: type of payment: made or missed.

            - date: payment date.

            - amount: amount of the payment made or missed in dollars.

        <b>Request Example</b>
        <pre><code>
            {
              "payment": "made",
              "date": "2017-09-05 02:18Z",
              "amount": 85.60
            }
        </code></pre>

        <b>Response Example</b>
        <pre><code>
            {
                "id": 1,
                "loan": 1,
                "payment": "made",
                "amount": "85.60",
                "date": "2017-09-05T02:18:00Z",
                "created_on": "2017-12-29T22:33:02.631321Z",
                "updated_on": "2017-12-29T22:33:02.631344Z"
            }
        </code></pre>

        """
        loan = get_object_or_404(Loan, id=pk)

        if not self._manually_check_permission(request, loan):
            return Response(
                {'detail': 'User cannot access this loan'},
                status=status.HTTP_403_FORBIDDEN
            )

        serializer = self.serializer_class(
            data=request.data, context={'loan': loan}
        )
        if serializer.is_valid():
            serializer.save(loan=loan)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(
        methods=['get'], authentication_classes=[JSONWebTokenAuthentication],
        permission_classes=[IsAuthenticated, DRYPermissions],
    )
    def balance(self, request, pk=None):
        """
        Get the volume of outstanding debt (i.e., debt yet to be paid) at some
        point in time.

        create:
        Creates a payment associated to a loan.

        <b>Parameters (not automatically generated):</b>

            - start_date: loan balance from this date.
            - end_date: loan balance until this date.

        <b>Response Example</b>
        <pre><code>
            {
                "balance": 40
            }
        </code></pre>

        """
        loan = get_object_or_404(Loan, id=pk)

        if not self._manually_check_permission(request, loan):
            return Response(
                {'detail': 'User cannot access this loan'},
                status=status.HTTP_403_FORBIDDEN
            )

        # self.serializer_class doesn't play well with DRYPermissions
        serializer = DateSerializer(data=request.query_params)
        if serializer.is_valid():

            view = LoanViewSet.as_view({'get': 'list'})
            # This bit is important for the filter backend
            view.action = 'list'

            # Let's reuse an existing filter backend and check permissions
            filter_backend = PaymentFilterBackend()
            payments = Payment.objects.filter(loan=loan, payment='missed')
            # This will filter the objects according to the user permissions.
            payments = filter_backend\
                .filter_list_queryset(request, payments, view)

            balance = payments.filter(
                date__gte=serializer.validated_data['start_date'],
                date__lte=serializer.validated_data['end_date']
            ).aggregate(Sum('amount'))
            if balance['amount__sum'] is not None:
                balance = balance['amount__sum']
            else:
                balance = 0
            return Response({'balance': balance}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(
        methods=['get'], authentication_classes=[JSONWebTokenAuthentication],
        permission_classes=[IsAuthenticated],
        serializer_class=DateSerializer
    )
    def total_amount(self, request):
        """
        Keep track of the amount of money loaned out.

        list:
        List total amount of loans.

        <b>Parameters (not automatically generated):</b>

            - start_date: loan balance until from date.
            - end_date: loan balance until this date.

        <b>Response Example</b>
        <pre><code>
            {
                "total_amount": 4300
            }
        </code></pre>

        """

        view = LoanViewSet.as_view({'get': 'list'})
        # This bit is important for the filter backend
        view.action = 'list'

        # Let's reuse an existing filter backend
        filter_backend = LoanFilterBackend()
        loans = Loan.objects.filter(is_accepted=True)
        # This will filter the objects according to the user permissions.
        loans = filter_backend.filter_list_queryset(request, loans, view)

        serializer = self.serializer_class(data=request.query_params)
        if serializer.is_valid():
            total_amount = loans.filter(
                date__gte=serializer.validated_data['start_date'],
                date__lte=serializer.validated_data['end_date']
            ).aggregate(Sum('amount'))

            if total_amount['amount__sum'] is not None:
                total_amount = total_amount['amount__sum']
            else:
                total_amount = 0

            return Response(
                {'total_amount': total_amount}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(
        methods=['get'], authentication_classes=[JSONWebTokenAuthentication],
        permission_classes=[IsAuthenticated, DRYPermissions],
    )
    def missed_made_payments(self, request, pk=None):
        """
        Keep track of the amount of the missed/made payments

        retrieve:
        retrieve the missed made payment counts associated to a loan.

        <b>Parameters (not automatically generated):</b>

            - start_date: loan balance until from date.
            - end_date: loan balance until this date.

        <b>Response Example</b>
        <pre><code>
            {
                "loan_id": "7348a345-2887-4db7-9a59-c33288d07e24",
                "made": 1,
                "missed": 1
            }
        </code></pre>

        """
        loan = get_object_or_404(Loan, id=pk)

        if not self._manually_check_permission(request, loan):
            return Response(
                {'detail': 'User cannot access this loan'},
                status=status.HTTP_403_FORBIDDEN
            )

        # self.serializer_class doesn't play well with DRYPermissions
        serializer = DateSerializer(data=request.query_params)
        if serializer.is_valid():
            view = LoanViewSet.as_view({'get': 'list'})
            # This bit is important for the filter backend
            view.action = 'list'

            # Let's reuse an existing filter backend and check permissions
            filter_backend = PaymentFilterBackend()
            payments = Payment.objects.filter(loan=loan)
            # This will filter the objects according to the user permissions.
            payments = filter_backend\
                .filter_list_queryset(request, payments, view)

            payments = payments.filter(
                date__gte=serializer.validated_data['start_date'],
                date__lte=serializer.validated_data['end_date']
            )
            made = payments.filter(payment='made').count()
            missed = payments.filter(payment='missed').count()

            return Response(
                {'loan_id': loan.loan_id, 'made': made, 'missed': missed},
                status=status.HTTP_200_OK
            )

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


router.register(r'loan', LoanViewSet)


