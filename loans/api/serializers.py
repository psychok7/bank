# -*- coding: utf-8 -*-

import logging


from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from loans.models import Loan, Payment

log = logging.getLogger('bank_console')


class LoanSerializer(serializers.ModelSerializer):
    # The user will be populated by the Token auth.
    user = serializers.PrimaryKeyRelatedField(read_only=True)
    installment = serializers.DecimalField(
        max_digits=10, decimal_places=2, read_only=True)

    class Meta:
        model = Loan
        fields = [
            'id', 'loan_id', 'installment', 'user', 'amount', 'term', 'rate',
            'date'
        ]

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return super(LoanSerializer, self).create(validated_data)


class PaymentSerializer(serializers.ModelSerializer):
    loan = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Payment
        fields = [
            'id', 'loan', 'payment', 'amount', 'date', 'created_on',
            'updated_on'
        ]

    def validate(self, data):
        payments = Payment.objects.filter(
            loan=self.context['loan'], date__month=data['date'].month,
            date__year=data['date'].year
        )
        if payments.exists():
            raise serializers.ValidationError(
                _(
                    'Loans are paid back in monthly installments, this month '
                    'has been paid'
                )
            )

        return data


class DateSerializer(serializers.Serializer):
    start_date = serializers.DateTimeField()
    end_date = serializers.DateTimeField()
