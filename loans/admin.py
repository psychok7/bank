# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Loan, Payment


class LoanAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'loan_id', 'user', 'installment', 'amount', 'term', 'rate',
        'date', 'created_on', 'updated_on'
    ]
    list_filter = ['created_on', 'updated_on']
    search_fields = ['loan_id']


admin.site.register(Loan, LoanAdmin)


class PaymentAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'loan', 'payment', 'amount', 'date', 'created_on', 'updated_on'
    ]
    list_filter = ['payment', 'created_on', 'updated_on']
    search_fields = ['loan__loan_id']


admin.site.register(Payment, PaymentAdmin)
