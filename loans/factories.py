# -*- coding: utf-8 -*-

import factory
import datetime

from factory.fuzzy import FuzzyDecimal

from loans.models import Loan, Payment
from users.factories import UserFactory


class LoanFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Loan
        django_get_or_create = ('loan_id',)

    user = factory.SubFactory(UserFactory)
    is_accepted = factory.Iterator([True, False])
    loan_id = factory.Faker('uuid4')
    amount = FuzzyDecimal(1000, 9000)
    term = factory.Faker('random_int')
    rate = FuzzyDecimal(0.01, 0.99)
    date = factory.LazyFunction(datetime.datetime.now)


class PaymentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Payment
        django_get_or_create = ('loan',)

    loan = factory.SubFactory(LoanFactory)
    payment = factory.Iterator(['missed', 'made'])
    amount = FuzzyDecimal(1000, 9000)
    date = factory.LazyFunction(datetime.datetime.now)

