# -*- coding: utf-8 -*-

import uuid

from django.core.urlresolvers import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from loans.factories import LoanFactory, PaymentFactory
from users.factories import UserFactory
from users.test_api import get_user_token


class LoanAPITests(TestCase):
    def setUp(self):
        self.client = APIClient(enforce_csrf_checks=True)
        self.user = UserFactory()
        self.another_user = UserFactory()

    def test_201_create_loan(self):
        """
        Ensure we can create a new Loan.
        """

        token = get_user_token(self)
        self.assertTrue(token)

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)

        data = {
            "amount": 1000, "term": 12, "rate": 0.05,
            "date": "2017-08-05 02:18Z",
        }
        response = self.client.post(reverse('loan-list'), data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        internal_loan_id = response.data.get('id')
        if internal_loan_id is None:
            self.fail("internal_loan_id was not generated!")

        loan_id = response.data.get('loan_id')
        if loan_id is None:
            self.fail("loan_id was not generated!")

        installment = response.data.get('installment')
        if installment is None:
            self.fail("installment was not generated!")

        loan = LoanFactory(loan_id=uuid.UUID(loan_id))

        self.assertEqual(response.data['id'], loan.id)
        self.assertEqual(response.data['loan_id'], str(loan.loan_id))
        self.assertEqual(response.data['installment'], str(loan.installment))
        self.assertEqual(response.data['installment'], '85.60')
        self.assertTrue(response.data)

        return internal_loan_id

    def test_201_create_missed_loan_payment(self):
        """
        Ensure we can create a new Loan.
        """

        internal_loan_id = self.test_201_create_loan()

        token = get_user_token(self)
        self.assertTrue(token)

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)

        data = {
            "payment": "missed", "date": "2017-09-05 02:18Z", "amount": 85.60,
        }
        response = self.client.post(
            reverse('loan-payments', kwargs={'pk': internal_loan_id}), data,
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        payment_id = response.data.get('id')
        if payment_id is None:
            self.fail("payment_id was not generated!")

        loan = response.data.get('loan')
        if loan is None:
            self.fail("installment was not generated!")

        payment = PaymentFactory(loan=loan)

        self.assertEqual(response.data['id'], payment.id)
        self.assertEqual(response.data['loan'], payment.loan.id)
        self.assertTrue(response.data)

        return internal_loan_id, payment_id

    def test_400_create_missed_loan_payment(self):
        """
        Ensure we can create a new Loan.
        """

        internal_loan_id = self.test_201_create_loan()

        token = get_user_token(self)
        self.assertTrue(token)

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)

        data = {
            "payment": "missed", "date": "2017-09-05 02:18Z", "amount": 85.60,
        }
        response = self.client.post(
            reverse('loan-payments', kwargs={'pk': internal_loan_id}), data,
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.post(
            reverse('loan-payments', kwargs={'pk': internal_loan_id}), data,
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(
            response.data['non_field_errors'],
            [
                'Loans are paid back in monthly installments, this month has '
                'been paid'
            ]
        )

    def test_403_create_missed_loan_payment(self):
        """
        Ensure we can create a new Loan.
        """

        internal_loan_id = self.test_201_create_loan()

        token = get_user_token(self)
        self.assertTrue(token)

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)

        data = {
            "payment": "missed", "date": "2017-09-05 02:18Z", "amount": 85.60,
        }
        response = self.client.post(
            reverse('loan-payments', kwargs={'pk': internal_loan_id}), data,
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Another user cannot create a payment for the current user.
        # Only a user that owns the loan can.
        token = get_user_token(self, another_user=True)
        self.assertTrue(token)

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)

        response = self.client.post(
            reverse('loan-payments', kwargs={'pk': internal_loan_id}), data,
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.assertEqual(
            response.data['detail'], 'User cannot access this loan'
        )

    def test_201_create_made_loan_payment(self):
        """
        Ensure we can create a new Loan.
        """

        internal_loan_id = self.test_201_create_loan()

        token = get_user_token(self)
        self.assertTrue(token)

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)

        data = {
            "payment": "made", "date": "2017-09-05 02:18Z", "amount": 85.60,
        }
        response = self.client.post(
            reverse('loan-payments', kwargs={'pk': internal_loan_id}), data,
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        payment_id = response.data.get('id')
        if payment_id is None:
            self.fail("payment_id was not generated!")

        loan = response.data.get('loan')
        if loan is None:
            self.fail("installment was not generated!")

        payment = PaymentFactory(loan=loan)

        self.assertEqual(response.data['id'], payment.id)
        self.assertEqual(response.data['loan'], payment.loan.id)
        self.assertTrue(response.data)

        return internal_loan_id, payment_id

    def test_200_get_loan_balance(self):
        """
        Ensure we can create a new Loan.
        """

        internal_loan_id, internal_payment_id = \
            self.test_201_create_missed_loan_payment()

        token = get_user_token(self)
        self.assertTrue(token)
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)

        start_date = '2016-09-05 02:18Z'
        end_date = '2018-09-05 02:18Z'

        response = self.client.get(
            reverse('loan-balance', kwargs={'pk': internal_loan_id})
            + f'?start_date={start_date}&end_date={end_date}', format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertTrue(response.data['balance'])

    def test_403_get_loan_balance(self):
        """
        Ensure we can create a new Loan.
        """

        internal_loan_id, internal_payment_id = \
            self.test_201_create_missed_loan_payment()

        token = get_user_token(self)
        self.assertTrue(token)
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)

        start_date = '2016-09-05 02:18Z'
        end_date = '2018-09-05 02:18Z'

        response = self.client.get(
            reverse('loan-balance', kwargs={'pk': internal_loan_id})
            + f'?start_date={start_date}&end_date={end_date}', format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        token = get_user_token(self, another_user=True)
        self.assertTrue(token)
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)

        response = self.client.get(
            reverse('loan-balance', kwargs={'pk': internal_loan_id})
            + f'?start_date={start_date}&end_date={end_date}', format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


    def test_200_get_loan_total_amount(self):
        """
        Ensure we can create a new Loan.
        """

        self.test_201_create_missed_loan_payment()
        self.test_201_create_made_loan_payment()

        token = get_user_token(self)
        self.assertTrue(token)
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)

        start_date = '2016-09-05 02:18Z'
        end_date = '2018-09-05 02:18Z'

        response = self.client.get(
            reverse('loan-total-amount')
            + f'?start_date={start_date}&end_date={end_date}', format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertTrue(response.data['total_amount'])

